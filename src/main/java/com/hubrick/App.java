package com.hubrick;

import io.vertx.core.Vertx;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(ClientVerticle.class.getName(), h->{
            System.out.printf("deployed %s\n", h.result());
        });
    }
}
