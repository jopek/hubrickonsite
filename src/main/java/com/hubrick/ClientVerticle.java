package com.hubrick;

import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.ext.web.client.HttpResponse;
import io.vertx.rxjava.ext.web.client.WebClient;
import io.vertx.rxjava.ext.web.codec.BodyCodec;
import rx.Observable;

import java.util.concurrent.atomic.AtomicReference;

public class ClientVerticle extends AbstractVerticle {
  @Override
  public void start() throws Exception {
    WebClient client = WebClient.create(vertx);
    AtomicReference<String> atom = new AtomicReference<>("");
    String apiToken = "Token beHN-Z2wNUtch4MUByhckWvCWtBYR8FW8Ah3kM9wpAM=";

    rx.Observable<HttpResponse<JsonObject>> repeat = rx.Observable.defer(
        () -> client.get("api.hubrick.io", "/api/v1/search/_search")
            .putHeader(HttpHeaders.AUTHORIZATION.toString(), apiToken)
            .ssl(true)
            .port(443)
            .addQueryParam("user.offset", atom.get())
            .addQueryParam("size", "25")
            .addQueryParam("filter", "user")
            .as(BodyCodec.jsonObject())
            .rxSend()
            .toObservable()
    )
        .repeat(40)
        .doOnNext(k -> atom.set(
            k.body()
                .getJsonObject("users")
                .getString("nextOffset")
            )
        )
        .takeUntil(c -> c.body()
            .getJsonObject("users")
            .getJsonArray("elements")
            .isEmpty());

    repeat
        .flatMap(entry -> Observable.from(
            entry.body()
                .getJsonObject("users")
                .getJsonArray("elements"))
        )
        .map(e -> (JsonObject) e)
        .map(e -> e.getJsonObject("entity"))
        .map(this::pullOutSpecificFields)
        .reduce(this::findLargerEntry)
        .subscribe(this::printEntry, this::crapOut);
  }

  private JsonObject pullOutSpecificFields(JsonObject entries) {
    JsonObject followerStats = entries.getJsonObject("followerStats");
    String name = entries.getString("name");
    Integer totalFollows = followerStats.getInteger("totalFollows");

    return new JsonObject()
        .put("name", name)
        .put("totalFollows", totalFollows);
  }

  private JsonObject findLargerEntry(JsonObject entry1, JsonObject entry2) {
    return entry1.getInteger("totalFollows") >= entry2.getInteger("totalFollows") ? entry1 : entry2;
  }

  private void printEntry(JsonObject entry) {
    String name = entry.getString("name");
    Integer followers = entry.getInteger("totalFollows");
    System.out.printf("%s %d\n", name, followers);
  }

  private void crapOut(Throwable throwable) {
    System.err.println("<<<  fuuuuu  >>>");
    throwable.printStackTrace();
  }

}
